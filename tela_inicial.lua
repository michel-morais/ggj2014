--[[----------------------------------------------------------------------------------------|
|*******************************************************************************************|
|-------------------------------------------------------------------------------------------|
|				Arquivo scene 1                                                             | 
|				BY SCENE MAKER version 1.0													| 
|				Mini Mbm version 1.0													    | 
|				E-Mail msn (michel.braz.morais@gmail.com)									| 
|-------------------------------------------------------------------------------------------|
|*******************************************************************************************|
-----------------------------------------------------------------------------------------]]--

function initScene()
	widthBackBufferSM , heightBackBufferSM = 1024.000000, 600.000000 --Largura e altura da tela esperada
	mbm:showConsole(true) --Console window
	mbm:addPath("D:\\Developer\\GameJam2014\\ceifador\\release\\assets")
	camera2d	= mbm:getCamera("2d")
	camera3d	= mbm:getCamera("3d")
	posCam2d	= camera2d:getPos()
	posCam3d	= camera3d:getPos()
	focusCam	= camera3d:getFocus()
	camera2d:scaleToScreen(widthBackBufferSM,heightBackBufferSM)
	mbm:setColorClear(1.000000,1.000000,1.000000)
	posCam2d:set(-1,-2)
	posCam3d:set(0,0,-500)
	focusCam:set(0,0,0)
	logoggjcwb = texture:new("2dw")
	logoggj = texture:new("2dw")
	--[[--------------------------------------------------------------------------------------]]
	if logoggjcwb:load("poster2.png") then
		logoggjcwb:setSize(555.750000,667.500000)
		logoggjcwb:setPos(0.000000, 0.000000,-1.050000)
		logoggjcwb:setAngle(0.000000,0.000000,0.000000)
		logoggjcwb:setScale(0.900000,0.900000,1.000000)
	else
		print("Falha ao carregar a Imagem [poster2.png]")
	end
	--[[--------------------------------------------------------------------------------------]]
	if logoggj:load("ggj14_fpweb_logo.png") then
		logoggj:setSize(354.000000,216.000000)
		logoggj:setPos(416.000000, -199.000000,-1.060000)
		logoggj:setAngle(0.000000,0.000000,0.000000)
		logoggj:setScale(1.000000,1.000000,1.000000)
	else
		print("Falha ao carregar a Imagem [ggj14_fpweb_logo.png]")
	end
	
	t1 = timer:new("onNextScene", 3.5)
end

function onNextScene(whatTimer)
	mbm:loadScene("menu_principal.lua")
end 

--[[--------------------------------------------------------------------------------------]]
function loop(fpsProp)
	fps 	= mbm:getFps()		--Frames por segundo
end

--[[--------------------------------------------------------------------------------------]]
function onTouchDown(key,x,y) --parameter: int key, float x, float y.


end

--[[--------------------------------------------------------------------------------------]]
function onTouchUp(key,x,y) --parameter: int key, float x, float y.


end

--[[--------------------------------------------------------------------------------------]]
function onTouchMove(x, y) --parameter: float x, float y.


end

--[[--------------------------------------------------------------------------------------]]
function onTouchZoom(zoom) --Parameter float zoom de -1 a +1.


end

--[[--------------------------------------------------------------------------------------]]
function onBackPress() --Ao pressionar a tecla esc


end

--[[--------------------------------------------------------------------------------------]]
function onMenuPress() --Ao pressionar o menu


end

--[[--------------------------------------------------------------------------------------]]
function endScene() --Metodo chamado ao encerrar a cena


end

--[[--------------------------------------------------------------------------------------]]
function onKeyDown(key) --parameter: int key.


end

--[[--------------------------------------------------------------------------------------]]
function onKeyUp(key) --parameter: int key.


end

--[[--------------------------------------------------------------------------------------]]

