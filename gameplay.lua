--[[----------------------------------------------------------------------------------------|
|*******************************************************************************************|
|-------------------------------------------------------------------------------------------|
|				Arquivo scene 1                                                             | 
|				BY SCENE MAKER version 1.0													| 
|				Mini Mbm version 1.0													    | 
|				E-Mail msn (michel.braz.morais@gmail.com)									| 
|-------------------------------------------------------------------------------------------|
|*******************************************************************************************|
-----------------------------------------------------------------------------------------]]--

function initScene()
	widthBackBufferSM , heightBackBufferSM = 1024.000000, 600.000000 --Largura e altura da tela esperada
	mbm:showConsole(true) --Console window
	mbm:addPath("E:\\C++\\sdk-mbm\\sceneMaker")
	mbm:addPath("C:\\Users\\Michel\\Desktop\\Ceifeiro\\cena1")
	mbm:addPath("E:\\C++\\sdk-mbm\\sceneMaker\\Debug")
	mbm:addPath("E:\\C++\\sdk-mbm\\sceneMaker\\Debug\\pixelShader")
	mbm:addPath("E:\\C++\\sdk-mbm\\sceneMaker\\Debug\\vertexShader")
	mbm:addPath("E:\\Dropbox\\Desenvolvimentos\\C++\\sdk-mbm\\sceneMaker")
	mbm:addPath("E:\\GamJam2014\\final\\asset")
	mbm:addPath("E:\\C++\\sdk-mbm\\mbmDx-lua")
	camera2d	= mbm:getCamera("2d")
	camera3d	= mbm:getCamera("3d")
	posCam2d	= camera2d:getPos()
	posCam3d	= camera3d:getPos()
	focusCam	= camera3d:getFocus()
	camera2d:scaleToScreen(widthBackBufferSM,heightBackBufferSM)
	mbm:setColorClear(0.000000,0.000000,0.000000)
	posCam2d:set(-305,-152)
	posCam3d:set(0,0,-500)
	focusCam:set(0,0,0)
	backGround_1 = texture:new("2dw")
	alvo = texture:new("2dw")
	sprite_1 = sprite:new("2dw")
	reaper = sprite:new("2dw")
	alvo:load("alvo.png")
	--[[--------------------------------------------------------------------------------------]]
	if backGround_1:load("gameplay_background.PNG") then
		backGround_1:setPos(0.000000,0.000000,10.000000)
		backGround_1.visible = true
	else
		print("Falha ao carregar o backGround [background.bmp]")
	end
	--[[--------------------------------------------------------------------------------------]]
	if reaper:load("reaper.mbm") then
		reaper:setPos(-677.000000,-91.000000,-1.250000)
		reaper:setAngle(0.000000,0.000000,0.000000)
		reaper:setScale(1.000000,1.000000,1.000000)
		reaper:setIndexAnim(0)
		reaper.visible=true
	else
		print("Falha ao carregar o Sprite [reaper.mbm]")
	end
	reaper.dest = vec2:new(reaper.x,reaper.y)
	reaper.normal = vec3:new()
	reaper.speed = 400
	reaper.speedCam = 200
	reaper.touch = false
	reaper.side = "right"
	reaper:onEndAnim("endAttack")
	reaper:setAnim("idle-right")
	reaper.isAttackOnEnemy = false
	
	lsEnemy = {}
	lsEnemy[0] = alvo
	vec2Calc = vec2:new()
	
	
end

function endAttack(obj,name)
	
	if name == "attack-right" or name == "attack-right-shader" then
		obj:setAnim("idle-right")
	elseif name == "attack-left" or name == "attack-left-shader" then
		obj:setAnim("idle-left")
	end
end

function cameraFollow()
	reaper.normal:set(reaper.x,reaper.y,0)
	reaper.normal:sub(camera2d.x,camera2d.y,0)

	if reaper.normal.len > 5 then
		reaper.normal:normalize()
		local x = 0
		local y = 0

		if posCam2d.x < -2424 then
			x = 1
		elseif posCam2d.x > 2424 then
			x = -1
		else
			x = reaper.normal.x * reaper.speedCam
		end

		if posCam2d.y < -1117 then
			y = 1
		elseif posCam2d.y > 1117 then
			y = -1
		else
			y = reaper.normal.y * reaper.speedCam
		end
		camera2d:move(x, y)
	end
end

function moveReaper()
	
	reaper.normal:set(reaper.x,reaper.y,0)
	reaper.normal:sub(reaper.dest.x,reaper.dest.y,0)
	
	if reaper.normal.len > 10 then
		reaper.normal:normalize()
		reaper:move(-reaper.normal.x * reaper.speed,-reaper.normal.y * reaper.speed)
	elseif reaper.touch then
		reaper.touch = false
		if reaper.isAttackOnEnemy then
			reaper.isAttackOnEnemy = false
			reaper:setAnim("attack-"..reaper.side.."-shader")
		else
			reaper:setAnim("attack-"..reaper.side)
		end
	end
end

--[[--------------------------------------------------------------------------------------]]
function loop(fpsProp)
	fps 	= mbm:getFps()		--Frames por segundo
	moveReaper()
	cameraFollow()
	
end

--[[--------------------------------------------------------------------------------------]]
function onTouchDown(key,x,y) --parameter: int key, float x, float y.
	x,y = mbm:to2dw(x,y)
	reaper.dest:set(x,y)
	if x < reaper.x then
		reaper:setAnim("idle-left")
		reaper.side="left"
	else
		reaper:setAnim("idle-right")
		reaper.side="right"
	end
	reaper.touch = true
	
	--Verifica se esta atacando o inimigo
	for index,value in pairs(lsEnemy) do
		vec2Calc:set(x,y)
		vec2Calc:sub(value.x,value.y)
		if vec2Calc.len < 100 then
			reaper.isAttackOnEnemy = true
		end
	end
end

--[[--------------------------------------------------------------------------------------]]
function onTouchUp(key,x,y) --parameter: int key, float x, float y.


end

--[[--------------------------------------------------------------------------------------]]
function onTouchMove(x, y) --parameter: float x, float y.


end

--[[--------------------------------------------------------------------------------------]]
function onTouchZoom(zoom) --Parameter float zoom de -1 a +1.


end

--[[--------------------------------------------------------------------------------------]]
function onBackPress() --Ao pressionar a tecla esc


end

--[[--------------------------------------------------------------------------------------]]
function onMenuPress() --Ao pressionar o menu


end

--[[--------------------------------------------------------------------------------------]]
function endScene() --Metodo chamado ao encerrar a cena


end

--[[--------------------------------------------------------------------------------------]]
function onKeyDown(key) --parameter: int key.


end

--[[--------------------------------------------------------------------------------------]]
function onKeyUp(key) --parameter: int key.


end

--[[--------------------------------------------------------------------------------------]]

