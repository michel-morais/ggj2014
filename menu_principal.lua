--[[----------------------------------------------------------------------------------------|
|*******************************************************************************************|
|-------------------------------------------------------------------------------------------|
|				Arquivo scene 1                                                             | 
|				BY SCENE MAKER version 1.0													| 
|				Mini Mbm version 1.0													    | 
|				E-Mail msn (michel.braz.morais@gmail.com)									| 
|-------------------------------------------------------------------------------------------|
|*******************************************************************************************|
-----------------------------------------------------------------------------------------]]--

function initScene()
	widthBackBufferSM , heightBackBufferSM = 1024.000000, 600.000000 --Largura e altura da tela esperada
	mbm:showConsole(true) --Console window
	mbm:addPath("D:\\Developer\\GameJam2014\\ceifador\\release\\assets")
	camera2d	= mbm:getCamera("2d")
	camera3d	= mbm:getCamera("3d")
	posCam2d	= camera2d:getPos()
	posCam3d	= camera3d:getPos()
	focusCam	= camera3d:getFocus()
	camera2d:scaleToScreen(widthBackBufferSM,heightBackBufferSM)
	mbm:setColorClear(0.000000,0.000000,0.000000)
	posCam2d:set(-7,-1)
	posCam3d:set(0,0,-500)
	focusCam:set(0,0,0)
	backGround_1 = backGround:new("2dw")
	btnPlay = texture:new("2ds")
	btnInstructions = texture:new("2ds")
	btnAbout = texture:new("2ds")
	--[[--------------------------------------------------------------------------------------]]
	if backGround_1:load("menuBackground.png") then
		backGround_1:setAngle(0.000000,0.000000,0.000000)
		backGround_1:setIndexAnim(0)
	else
		print("Falha ao carregar o backGround [menuBackground.png]")
	end
	--[[--------------------------------------------------------------------------------------]]
	if btnPlay:load("btnPlay.png", 1, 1, 1) then
		btnPlay:setSize(288.000000,104.000000)
		btnPlay:setPos(844.000000, 352.000000,-1.070000)
		btnPlay:setAngle(0.000000,0.000000,0.000000)
		btnPlay:setScale(1.000000,1.000000,1.000000)
	else
		print("Falha ao carregar a Imagem [btnPlay.png]")
	end
	--[[--------------------------------------------------------------------------------------]]
	if btnInstructions:load("btnInstructions.png", 1, 1, 1) then
		btnInstructions:setSize(288.000000,100.000000)
		btnInstructions:setPos(844.000000, 447.000000,-1.080000)
		btnInstructions:setAngle(0.000000,0.000000,0.000000)
		btnInstructions:setScale(1.000000,1.000000,1.000000)
	else
		print("Falha ao carregar a Imagem [btnInstructions.png]")
	end
	--[[--------------------------------------------------------------------------------------]]
	if btnAbout:load("btnAbout.png", 1, 1, 1) then
		btnAbout:setSize(288.000000,98.000000)
		btnAbout:setPos(845.000000, 535.000000,-1.090000)
		btnAbout:setAngle(0.000000,0.000000,0.000000)
		btnAbout:setScale(1.000000,1.000000,1.000000)
	else
		print("Falha ao carregar a Imagem [btnAbout.png]")
	end
end

--[[--------------------------------------------------------------------------------------]]
function loop(fpsProp)
	fps 	= mbm:getFps()		--Frames por segundo
end

--[[--------------------------------------------------------------------------------------]]
function onTouchDown(key,x,y) --parameter: int key, float x, float y.
	mbm:loadScene("gameplay.lua")
end

--[[--------------------------------------------------------------------------------------]]
function onTouchUp(key,x,y) --parameter: int key, float x, float y.


end

--[[--------------------------------------------------------------------------------------]]
function onTouchMove(x, y) --parameter: float x, float y.


end

--[[--------------------------------------------------------------------------------------]]
function onTouchZoom(zoom) --Parameter float zoom de -1 a +1.


end

--[[--------------------------------------------------------------------------------------]]
function onBackPress() --Ao pressionar a tecla esc


end

--[[--------------------------------------------------------------------------------------]]
function onMenuPress() --Ao pressionar o menu


end

--[[--------------------------------------------------------------------------------------]]
function endScene() --Metodo chamado ao encerrar a cena


end

--[[--------------------------------------------------------------------------------------]]
function onKeyDown(key) --parameter: int key.


end

--[[--------------------------------------------------------------------------------------]]
function onKeyUp(key) --parameter: int key.


end

--[[--------------------------------------------------------------------------------------]]

